(ns dataloadertimescaledb2.DataStructure)

(def globalConfig (array-map
                    :startDate "2040-01-01"
                    :endDate "2047-01-02"
                    :F12H "5 minutes"
                    :F4H "15 minutes"
                    :F1H "1 hours"
                    :F4D "6 hours"
                    :F2D "12 hours"
                    :F1D "1 day"
                    :F1M "1 month"))

(def timeSeriesConfig_Map (array-map
                            ;:TSGridForecast {:quantity         20 :config {
                            ;                                               :F12H {
                            ;                                                      :pourcentage         0
                            ;                                                      :chunk_time_interval "interval ''1 days''"
                            ;                                                      }
                            ;                                               :F4H  {
                            ;                                                      :pourcentage         0
                            ;                                                      :chunk_time_interval "interval ''2 days''"}
                            ;                                               :F1H  {
                            ;                                                      :pourcentage         10
                            ;                                                      :chunk_time_interval "interval ''2 days''"}
                            ;                                               :F4D  {
                            ;                                                      :pourcentage         80
                            ;                                                      :chunk_time_interval "interval ''4 days''"}
                            ;                                               :F2D  {
                            ;                                                      :pourcentage         10
                            ;                                                      :chunk_time_interval "interval ''5 days''"}
                            ;                                               :F1D  {
                            ;                                                      :pourcentage         0
                            ;                                                      :chunk_time_interval "interval ''6 days''"}
                            ;                                               :F1M  {
                            ;                                                      :pourcentage         0
                            ;                                                      :chunk_time_interval "interval ''7 days''"}
                            ;                                               }
                            ;                 :schemaCreate     "id uuid NOT NULL,\n\t\t   observationdatetime timestamp without time zone UNIQUE NOT NULL,\n\t\t   value double precision,\n\t\t   collectiondatetime timestamp without time zone"
                            ;                 :insertIntoSchema "( id, observationdatetime, value, collectiondatetime )\n\tSELECT uuid_generate_v4() as id,\n\t\t\tobservationdatetime,\n\t\t\trandom() as value,\n\t\t\tnow() as collectionDateTime FROM generate_series"
                            ;                 }
                            ;:TSTVPQuantForecast {:quantity         2500 :config {
                            ;                                                     :F12H {
                            ;                                                            :pourcentage         0
                            ;                                                            :chunk_time_interval "interval ''1 days''"
                            ;                                                            }
                            ;                                                     :F4H  {
                            ;                                                            :pourcentage         0
                            ;                                                            :chunk_time_interval "interval ''2 days''"}
                            ;                                                     :F1H  {
                            ;                                                            :pourcentage         10
                            ;                                                            :chunk_time_interval "interval ''2 days''"}
                            ;                                                     :F4D  {
                            ;                                                            :pourcentage         80
                            ;                                                            :chunk_time_interval "interval ''4 days''"}
                            ;                                                     :F2D  {
                            ;                                                            :pourcentage         10
                            ;                                                            :chunk_time_interval "interval ''5 days''"}
                            ;                                                     :F1D  {
                            ;                                                            :pourcentage         0
                            ;                                                            :chunk_time_interval "interval ''6 days''"}
                            ;                                                     :F1M  {
                            ;                                                            :pourcentage         0
                            ;                                                            :chunk_time_interval "interval ''7 days''"}
                            ;                                                     }
                            ;                     :schemaCreate     "id uuid NOT NULL,\n\t\t   observationdatetime timestamp without time zone UNIQUE NOT NULL,\n\t\t   value double precision,\n\t\t   collectiondatetime timestamp without time zone,"
                            ;                     :insertIntoSchema "( id, observationdatetime, value, collectiondatetime )\n\tSELECT uuid_generate_v4() as id,\n\t\t\tobservationdatetime,\n\t\t\trandom() as value,\n\t\t\tnow() as collectionDateTime FROM generate_series"
                            ;                     }
  :TSTVPCorrectableQual {:quantity         20 :config {
                                                       :F12H {
                                                              :pourcentage         30
                                                              :chunk_time_interval "interval ''2 days''"
                                                              }
                                                       :F4H  {
                                                              :pourcentage         20
                                                              :chunk_time_interval "interval ''4 days''"}
                                                       :F1H  {
                                                              :pourcentage         35
                                                              :chunk_time_interval "interval ''6 days''"}
                                                       :F4D  {
                                                              :pourcentage         0
                                                              :chunk_time_interval "interval ''25 days''"}
                                                       :F2D  {
                                                              :pourcentage         0
                                                              :chunk_time_interval "interval ''100 days''"}
                                                       :F1D  {
                                                              :pourcentage         10
                                                              :chunk_time_interval "interval ''599 days''"}
                                                       :F1M  {
                                                              :pourcentage         5
                                                              :chunk_time_interval "interval ''17960 days''"}
                                                       }
                         :schemaCreate     "id uuid NOT NULL,\n  collectiondatetime timestamp without time zone,\n observationdatetime timestamp without time zone UNIQUE NOT NULL,\n latest boolean,\n  value text,\n comment jsonb,\n qualityControlStatus text,\n qualityControlLevel text,\n  author text"
                         :insertIntoSchema "( id, collectiondatetime, observationdatetime, latest, value, comment, qualityControlStatus, qualityControlLevel, author )\n  SELECT\n uuid_generate_v4() as id,\n now() as collectionDateTime,\n  observationdatetime,\n  ''true'' as latest,\n substr(md5(random()::text), 0, 14) as value,\n json_build_object(''fr'',substr(md5(random()::text), 0, 47),''en'',substr(md5(random()::text), 0, 47)) as comment,\n  substr(md5(random()::text), 0, 14) as qualityControlStatus,\n substr(md5(random()::text), 0, 14) as qualityControlLevel,\n substr(md5(random()::text), 0, 14) as author\n FROM generate_series"
                         }
  :TSTVPCorrectableQuant {:quantity         800 :config {
                                                         :F12H {
                                                                :pourcentage         30
                                                                :chunk_time_interval "interval ''2 days''"
                                                                }
                                                         :F4H  {
                                                                :pourcentage         20
                                                                :chunk_time_interval "interval ''4 days''"}
                                                         :F1H  {
                                                                :pourcentage         35
                                                                :chunk_time_interval "interval ''6 days''"}
                                                         :F4D  {
                                                                :pourcentage         0
                                                                :chunk_time_interval "interval ''25 days''"}
                                                         :F2D  {
                                                                :pourcentage         0
                                                                :chunk_time_interval "interval ''100 days''"}
                                                         :F1D  {
                                                                :pourcentage         10
                                                                :chunk_time_interval "interval ''599 days''"}
                                                         :F1M  {
                                                                :pourcentage         5
                                                                :chunk_time_interval "interval ''17960 days''"}
                                                         }
                          :schemaCreate     "id uuid NOT NULL,\n  collectiondatetime timestamp without time zone,\n observationdatetime timestamp without time zone UNIQUE NOT NULL,\n latest boolean,\n  value double precision,\n comment jsonb,\n qualityControlStatus text,\n qualityControlLevel text,\n  author text"
                          :insertIntoSchema "( id, collectiondatetime, observationdatetime, latest, value, comment, qualityControlStatus, qualityControlLevel, author )\n  SELECT\n uuid_generate_v4() as id,\n now() as collectionDateTime,\n  observationdatetime,\n  ''true'' as latest,\n random()*99+1 as value,\n json_build_object(''fr'',substr(md5(random()::text), 0, 47),''en'',substr(md5(random()::text), 0, 47)) as comment,\n  substr(md5(random()::text), 0, 14) as qualityControlStatus,\n substr(md5(random()::text), 0, 14) as qualityControlLevel,\n substr(md5(random()::text), 0, 14) as author\n FROM generate_series"
                          }
  :TSGrid {:quantity         20 :config {
                                         :F12H {
                                                :pourcentage         30
                                                :chunk_time_interval "interval ''9 days''"
                                                }
                                         :F4H  {
                                                :pourcentage         20
                                                :chunk_time_interval "interval ''17 days''"}
                                         :F1H  {
                                                :pourcentage         35
                                                :chunk_time_interval "interval ''26 days''"}
                                         :F4D  {
                                                :pourcentage         0
                                                :chunk_time_interval "interval ''102 days''"}
                                         :F2D  {
                                                :pourcentage         0
                                                :chunk_time_interval "interval ''408 days''"}
                                         :F1D  {
                                                :pourcentage         10
                                                :chunk_time_interval "interval ''2449 days''"}
                                         :F1M  {
                                                :pourcentage         5
                                                :chunk_time_interval "interval ''73472 days''"}
                                         }
           :schemaCreate     "id uuid NOT NULL,\n\t\t   observationdatetime timestamp without time zone UNIQUE NOT NULL,\n\t\t   value text,\n\t\t   collectiondatetime timestamp without time zone"
           :insertIntoSchema "( id, observationdatetime, value, collectiondatetime )\n\tSELECT uuid_generate_v4() as id,\n\t\t\tobservationdatetime,\n\t\t\tsubstr(md5(random()::text), 0, 47),\n\t\t\tnow() as collectionDateTime FROM generate_series"
           }
  :TSTVPQual {:quantity         100 :config {
                                             :F12H {
                                                    :pourcentage         30
                                                    :chunk_time_interval "interval ''11 days''"
                                                    }
                                             :F4H  {
                                                    :pourcentage         20
                                                    :chunk_time_interval "interval ''23 days''"}
                                             :F1H  {
                                                    :pourcentage         35
                                                    :chunk_time_interval "interval ''34 days''"}
                                             :F4D  {
                                                    :pourcentage         0
                                                    :chunk_time_interval "interval ''136 days''"}
                                             :F2D  {
                                                    :pourcentage         0
                                                    :chunk_time_interval "interval ''544 days''"}
                                             :F1D  {
                                                    :pourcentage         10
                                                    :chunk_time_interval "interval ''3265 days''"}
                                             :F1M  {
                                                    :pourcentage         5
                                                    :chunk_time_interval "interval ''97962 days''"}
                                             }
              :schemaCreate     "id uuid NOT NULL,\n\t\t   observationdatetime timestamp without time zone UNIQUE NOT NULL,\n\t\t   value text,\n\t\t   collectiondatetime timestamp without time zone"
              :insertIntoSchema "( id, observationdatetime, value, collectiondatetime )\n\tSELECT uuid_generate_v4() as id,\n\t\t\tobservationdatetime,\n\t\t\tsubstr(md5(random()::text), 0, 14) as value,\n\t\t\tnow() as collectionDateTime FROM generate_series"
              }
  :TSTVPQuant {:quantity         500 :config {
                                              :F12H {
                                                     :pourcentage         30
                                                     :chunk_time_interval "interval ''12 days''"
                                                     }
                                              :F4H  {
                                                     :pourcentage         20
                                                     :chunk_time_interval "interval ''2 days''"}
                                              :F1H  {
                                                     :pourcentage         35
                                                     :chunk_time_interval "interval ''25 days''"}
                                              :F4D  {
                                                     :pourcentage         0
                                                     :chunk_time_interval "interval ''148 days''"}
                                              :F2D  {
                                                     :pourcentage         0
                                                     :chunk_time_interval "interval ''592 days''"}
                                              :F1D  {
                                                     :pourcentage         10
                                                     :chunk_time_interval "interval ''3552 days''"}
                                              :F1M  {
                                                     :pourcentage         5
                                                     :chunk_time_interval "interval ''106574 days''"}
                                              }
               :schemaCreate     "id uuid NOT NULL,\n\t\t   observationdatetime timestamp without time zone UNIQUE NOT NULL,\n\t\t   value double precision,\n\t\t   collectiondatetime timestamp without time zone"
               :insertIntoSchema "( id, observationdatetime, value, collectiondatetime )\n\tSELECT uuid_generate_v4() as id,\n\t\t\tobservationdatetime,\n\t\t\ttrunc((random() * 99 + 1)::numeric,4) as value,\n\t\t\tnow() as collectionDateTime FROM generate_series"
               }
                            )
  )
