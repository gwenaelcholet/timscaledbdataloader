(ns dataloadertimescaledb2.core
  (:use [clostache.parser]
        [dataloadertimescaledb2.DataStructure]))

(defn getName [oneTimeSeriesConfig] (name (key oneTimeSeriesConfig)))
(defn getConfig [oneTimeSeriesConfig] (get (nth oneTimeSeriesConfig 1) :config))
(defn getQuantity [oneTimeSeriesConfig] (get (nth oneTimeSeriesConfig 1) :quantity))
(defn getSchemaCreate [oneTimeSeriesConfig] (get (nth oneTimeSeriesConfig 1) :schemaCreate))
(defn getInsertIntoSchema [oneTimeSeriesConfig] (get (nth oneTimeSeriesConfig 1) :insertIntoSchema))
(defn getPeriodicity [periodicityConfig] (name (key periodicityConfig)))
(defn getPeriodicityDetails [periodicityConfig] (nth periodicityConfig 1))

(defn getHypertablePrefix [name periodicity] (str name "_" periodicity "_"))
(defn getQuantityPerPeriodicity [quantity periodicityPourcentage] (/ (* quantity periodicityPourcentage) 100))


(defn spitSql [fileName sqlContent] (spit (str "resources/output/" fileName ".sql") sqlContent))

(defn sqlContent [name periodicity periodicityDetails globalConfig schemaCreate insertIntoSchema quantityPerPeriodicity] (
                                                        render-resource (str "templates/SQL.mustache") {:frequency           ((keyword periodicity) globalConfig)
                                                                                                        :amount              quantityPerPeriodicity
                                                                                                        :hypertablePrefix    (getHypertablePrefix name periodicity)
                                                                                                        :chunk_time_interval (:chunk_time_interval periodicityDetails)
                                                                                                        :schemaCreate        schemaCreate
                                                                                                        :insertIntoSchema    insertIntoSchema
                                                                                                        :startDate           (:startDate globalConfig)
                                                                                                        :endDate             (:endDate globalConfig)}))

(defn sqlPerSerieSAndPeriodicity [name quantity periodicity periodicityDetails schemaCreate insertIntoSchema globalConfig] (let [quantityPerPeriodicity (getQuantityPerPeriodicity quantity (:pourcentage periodicityDetails))]
                                                                                                                             (if (> quantityPerPeriodicity 0)
                                                                                                                               (do
                                                                                                                                 (let [sqlContent (sqlContent name periodicity periodicityDetails globalConfig schemaCreate insertIntoSchema quantityPerPeriodicity)
                                                                                                                                       fileName (str name "_" periodicity)]
                                                                                                                                   (spitSql fileName sqlContent)
                                                                                                                                   (str fileName)))
                                                                                                                               nil)))

(defn runPeriodicityForASeriesType [name config quantity schemaCreate insertIntoSchema globalConfig]
      (loop [sql ""
             name name
             config config
             quantity quantity
             schemaCreate schemaCreate
             insertIntoSchema insertIntoSchema
             globalConfig globalConfig]
        (if (empty? config)
          sql
          (recur (str sql (sqlPerSerieSAndPeriodicity name
                                                      quantity
                                                      (getPeriodicity (first config))
                                                      (getPeriodicityDetails (first config))
                                                      schemaCreate
                                                      insertIntoSchema
                                                      globalConfig) "-") name (rest config) quantity schemaCreate insertIntoSchema globalConfig))))

(defn runTimeSeriesType [timeSeriesConfig globalConfig]
      (loop [sql ""
             timeSeriesConfig timeSeriesConfig
             globalConfig globalConfig]
        (if (empty? timeSeriesConfig)
          sql
          (recur (str sql (runPeriodicityForASeriesType
                            (getName (first timeSeriesConfig))
                            (getConfig (first timeSeriesConfig))
                            (getQuantity (first timeSeriesConfig))
                            (getSchemaCreate (first timeSeriesConfig))
                            (getInsertIntoSchema (first timeSeriesConfig))
                            globalConfig)) (rest timeSeriesConfig) globalConfig))))

(defn -main []
      (runTimeSeriesType timeSeriesConfig_Map globalConfig))

