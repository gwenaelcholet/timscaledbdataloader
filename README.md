# dataloadertimescaledb2

A Clojure library designed to generate the data volume to benchmark.

###to run the project
Set up the data map within DataStructure.clj    
This data map contains the keys to swap within the template file located: ./resources/templates/SQL.moustache

To run the project using the command-line:  
>lein run  

Output file generated there: ./resources/output/

## Reminder

### To create the data base:
CREATE DATABASE trial;
\c trial
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

### To request hypertable tables:
SELECT * FROM hypertable_approximate_row_count();   
SELECT show_chunks();   
SELECT show_chunks(older_than => interval '3 months');  
SELECT show_chunks(newer_than => now() + interval '3 months');  
SELECT show_chunks('conditions', older_than => interval '3 months');    
SELECT show_chunks('conditions', older_than => '2017-01-01'::date);     
SELECT show_chunks(newer_than => interval '3 months');    
SELECT show_chunks(older_than => interval '3 months', newer_than => interval '4 months');   
SELECT * FROM timescaledb_information.hypertable;   
SELECT * FROM timescaledb_information.compressed_chunk_stats;   
SELECT * FROM timescaledb_information.compressed_hypertable_stats;  
SELECT * FROM chunk_relation_size('quantvalue12h_10');  
SELECT * FROM hypertable_relation_size('quantvalue12h_10');  
SELECT * FROM chunk_relation_size_pretty('quantvalue12h_10');   
SELECT * FROM hypertable_relation_size_pretty('quantvalue12h_10');  
SELECT * FROM indexes_relation_size_pretty('quantvalue12h_10'); 

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
